from enum import Enum
from typing import List
import pygame
from os import path
import config

# точки поворота с направлением поворота
class RotatePoint:
    def __init__(self, x, y, direction):
        self.x = x
        self.y = y
        self.direction = direction

# чтобы управлять змейкой
class Direction(Enum):
    UP = 1
    DOWN = 2
    RIGHT = 3
    LEFT = 4

# директория с картинками
img_dir = path.join(path.dirname(__file__), 'pictures')

# словарь с текстурами и головой змейки
snake_objects = dict(
    head_img=pygame.image.load(img_dir+ '\head_0.png').convert_alpha(),
    worm_img=pygame.image.load(img_dir+ '\worm_1.png').convert_alpha())

# блоки, из которых состоит змейка
class Block(pygame.sprite.Sprite):
    def __init__(self, direction, x, y, nextBlock=None, image="worm_img"):
        pygame.sprite.Sprite.__init__(self)
        self.direction = None
        self.image = snake_objects[image]
        self.rect = self.image.get_rect()
        self.rect.width = config.BLOCKSIZE
        self.rect.height = config.BLOCKSIZE

        self.nextBlock = nextBlock
        self.changeDirection(direction)

        self.rect.x = x
        self.rect.y = y
        self.rotatePoints: List[RotatePoint] = []

        self.image = pygame.transform.scale(self.image, (config.BLOCKSIZE, config.BLOCKSIZE))
        self.image = pygame.transform.rotate(self.image,270)


    def changeDirection(self, direction):
    # нельзя поворачивать в противоположную сторону, например LEFT -> RIGHT
        print(direction.value)
        if (direction == Direction.RIGHT and self.direction == Direction.LEFT):
            return
        if (direction == Direction.LEFT and self.direction == Direction.RIGHT):
            return
        if (direction == Direction.UP and self.direction == Direction.DOWN):
            return
        if (direction == Direction.DOWN and self.direction == Direction.UP):
            return

        prev_dir = self.direction
        self.direction = direction

        # проверяем, наличие след. блока и добавляем точку поворота в список для него
        if self.nextBlock is not None:
            self.nextBlock.rotatePoints.append(RotatePoint(self.rect.x, self.rect.y, direction))

        # повороты змейки
        if direction == Direction.UP:
            self.dx, self.dy = 0, -5
            if prev_dir == Direction.LEFT:
                self.image = pygame.transform.rotate(self.image, 270)
            elif prev_dir == Direction.RIGHT:
                self.image = pygame.transform.rotate(self.image, 90)

        elif direction == Direction.DOWN:
            self.dx, self.dy = 0, 5
            if prev_dir == Direction.LEFT:
                self.image = pygame.transform.rotate(self.image, 90)
            elif prev_dir == Direction.RIGHT:
                self.image = pygame.transform.rotate(self.image, 270)

        elif direction == Direction.RIGHT:
            self.dx, self.dy = 5, 0
            if prev_dir == Direction.UP:
                self.image = pygame.transform.rotate(self.image, 270)
            elif prev_dir == Direction.DOWN:
                self.image = pygame.transform.rotate(self.image, 90)

        elif direction == Direction.LEFT:
            self.dx, self.dy = -5, 0
            if prev_dir == Direction.UP:
                self.image = pygame.transform.rotate(self.image, 90)
            elif prev_dir == Direction.DOWN:
                self.image = pygame.transform.rotate(self.image, 270)


    # создание следующего блока
    def setNextBlock(self, block):
        self.nextBlock = block

    # обновление змейки, проверка на поворот
    def update(self):
        self.rect.x += self.dx
        self.rect.y += self.dy
        for point in self.rotatePoints:
            if (abs(point.x - self.rect.x) <= 2) and (abs(point.y - self.rect.y) <= 2):
                self.rect.x = point.x
                self.rect.y = point.y
                self.changeDirection(point.direction)
                self.rotatePoints.remove(point)
                break

# класс змейки
class Snake:
    def __init__(self):
        # звук, когда змейка налетает на объект с поля
        self.sound_eat = pygame.mixer.Sound('eat.ogg')
        self.blocks: List[Block] = []
        self.sprites = pygame.sprite.Group()
        self.addCurrBlock(Block(Direction.DOWN, config.WIDTH / 2, config.HEIGHT / 2, image="head_img"))
        for i in range(1, 20):
            self.addBlock()

    def addCurrBlock(self, block: Block):
        self.blocks.append(block)
        self.sprites.add(block)
        if len(self.blocks) == 1:
            return
        self.blocks[len(self.blocks) - 2].setNextBlock(block)

    def addBlock(self):
        dx, dy = 0, 0
        last_block = self.blocks[len(self.blocks) - 1]
        if last_block.direction == Direction.UP:
            dx, dy = 0, config.BLOCKSIZE
        elif last_block.direction == Direction.DOWN:
            dx, dy = 0, -config.BLOCKSIZE
        elif last_block.direction == Direction.RIGHT:
            dx, dy = -config.BLOCKSIZE, 0
        elif last_block.direction == Direction.LEFT:
            dx, dy = config.BLOCKSIZE, 0

        block = Block(last_block.direction, last_block.rect.x + dx, last_block.rect.y + dy)
        last_block.setNextBlock(block)
        self.blocks.append(block)
        self.sprites.add(block)

    def deleteBlock(self, idx):
        idx += 1
        for i in range(idx, len(self.blocks)):
            self.sprites.remove(self.blocks[i])

        while len(self.blocks) != idx:
            self.blocks.remove(self.blocks[idx])
        self.blocks[idx - 1].setNextBlock(None)

    def update(self):
        self.sprites.update()
        for i in range(2, len(self.blocks) - 1):
            if self.blocks[0].rect.colliderect(self.blocks[i].rect):
                self.deleteBlock(i)
                break

    def draw(self, screen):
        self.sprites.draw(screen)

    def changeDirection(self, direction: Direction):
        if self.blocks[0].rect.colliderect(self.blocks[1]):
            return
        self.blocks[0].changeDirection(direction)
