from distutils.command.config import config

import pygame
import random
from os import path
import snake
import randomdict
import config

img_dir = path.join(path.dirname(__file__), 'pictures')

trivial_objects = randomdict.RandomDict(
    pizza_img=pygame.image.load(path.join(img_dir, "pizza.png")).convert_alpha(),
    borgar_img=pygame.image.load(path.join(img_dir, "borgar.png")).convert_alpha(),
    carrot_img=pygame.image.load(path.join(img_dir, "carrot.png")).convert_alpha(),
    steak_img=pygame.image.load(path.join(img_dir, "steak.png")).convert_alpha(),
    icecream_img=pygame.image.load(path.join(img_dir, "icecream.png")).convert_alpha(),
    puncakes_img=pygame.image.load(path.join(img_dir, "puncakes.png")).convert_alpha(),
    puncakes1_img=pygame.image.load(path.join(img_dir, "puncakes_1.png")).convert_alpha())


class Object(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = trivial_objects.random_value()
        self.rect = self.image.get_rect()
        self.rect.x = random.randint(config.FOODSIZE, config.WIDTH - config.FOODSIZE)
        self.rect.y = random.randint(config.FOODSIZE, config.HEIGHT - config.FOODSIZE)
        self.rect.height = config.FOODSIZE
        self.rect.width = config.FOODSIZE
        self.image = pygame.transform.scale(self.image, (config.FOODSIZE, config.FOODSIZE))


All_Objects = pygame.sprite.Group()
All_ObjectsL = []

seconds_before_spawn = 3

class AllObjects(Object):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.seconds_before_spawn = config.FOODSPAWNRATE


    def GenerateObject(self):

        while True:
            newObj = Object()
            All_Objects.add(newObj)
            All_ObjectsL.append(newObj)
            return All_Objects


    def deleteObject(self, one_object):
        All_Objects.remove( one_object)
        All_ObjectsL.remove(one_object)


    def updateObjects(self,ply: snake.Snake):
        self.seconds_before_spawn -= 1/30
        if self.seconds_before_spawn <= 0:
            self.seconds_before_spawn = config.FOODSPAWNRATE
            self.GenerateObject()
        for i in range(0, len(All_ObjectsL)):
            if ply.blocks[0].rect.colliderect(All_ObjectsL[i].rect):
                ply.addBlock()
                self.deleteObject(All_ObjectsL[i])
                ply.sound_eat.play()
                break

    def draw(self, screen):
        All_Objects.draw(screen)
