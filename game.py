import config
import pygame
from pygame.locals import *

def toggle_fullscreen():
    screen = pygame.display.get_surface()
    tmp = screen.convert()
    caption = pygame.display.get_caption()
    cursor = pygame.mouse.get_cursor()  # Duoas 16-04-2007

    w, h = screen.get_width(), screen.get_height()
    flags = screen.get_flags()
    bits = screen.get_bitsize()

    pygame.display.quit()
    pygame.display.init()

    screen = pygame.display.set_mode((w, h), flags ^ FULLSCREEN, bits)
    screen.blit(tmp, (0, 0))
    pygame.display.set_caption(*caption)

    pygame.key.set_mods(0)  # HACK: work-a-round for a SDL bug??

    pygame.mouse.set_cursor(*cursor)  # Duoas 16-04-2007

    return screen

pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((config.WIDTH, config.HEIGHT))
pygame.display.set_caption("WORM's LIFE")
clock = pygame.time.Clock()
toggle_fullscreen()
screen = pygame.display.get_surface()


import snake

pygame.mixer.music.load('ambient.wav')
pygame.mixer.music.set_volume(0.5)
pygame.mixer.music.play(loops=100)

import field_objects

player = snake.Snake()
#playersprite = pygame.sprite.RenderPlain(player)

background = pygame.image.load(field_objects.img_dir+ '\ground.png').convert()
background_rect = background.get_rect()

all_objs = field_objects.AllObjects()
# Цикл игры
running = True
while running:

    # Держим цикл на правильной скорости
    clock.tick(config.FPS)
    # Ввод процесса (события)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == 273:
                player.changeDirection(snake.Direction.UP)
            elif event.key == 274:
                player.changeDirection(snake.Direction.DOWN)
            elif event.key == 275:
                player.changeDirection(snake.Direction.RIGHT)
            elif event.key == 276:
                player.changeDirection(snake.Direction.LEFT)
            elif event.key == 27:
                running = False
            elif event.key == pygame.K_SPACE:
                for i in range(5):
                    all_objs.GenerateObject()

    # Обновление
     player.update()


    # Рендеринг
    screen.fill(config.WHITE)
    screen.blit(background, background_rect)
    player.draw(screen)

    all_objs.updateObjects(ply=player)
    all_objs.draw(screen)
    pygame.display.flip()

pygame.quit()
